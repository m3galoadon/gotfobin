package main

/*
	---------------------------------------
	Simple CLI tool to search onto GTFObin.
	M3GALOADON --------------------- 2022 -
	---------------------------------------
*/

import (
	"GOtfo/pkg/core"
	"GOtfo/pkg/core/bins"
	"GOtfo/pkg/core/visuals"
	"os"
	"strconv"
)

// Global variables				///////////////////////////////////////////////////////////////////////////////////////////////////////////
// MODE							~~~~~~~~~~~~~~~~~~~~~~~~
var MODE int = 0

// MODES TYPES 					~~~~~~~~~~~~~~~~~~~~~~~~
const (
	M_SEARCH      = 1
	M_SEARCH_FLAG = 2
	M_LOOK        = 3
)

// Main function				///////////////////////////////////////////////////////////////////////////////////////////////////////////
func main() {
	// init						~~~~~~~~~~~~~~~~~~~~~~~~
	var gathering bool = false  // is the program gathering search
	var exact_mode bool = false // exact mode
	var search string = ""      // the search tag of the program

	// get the arguments		~~~~~~~~~~~~~~~~~~~~~~~~
	if len(os.Args[1:]) < 1 {
		// check there is at least one arg (--help / --version)
		visuals.Error("Wrong usage!")
		core.Help()
		return
	}

	// Miscelanious				~~~~~~~~~~~~~~~~~~~~~~~~
	for _, a := range os.Args[1:] {

		switch a {
		case "--help":
			// help				----------------
			core.Help()
			return
		case "--version":
			// version 			----------------
			core.Version()
			return
		}
	}

	// get the arguments		~~~~~~~~~~~~~~~~~~~~~~~~
	if len(os.Args[1:]) < 2 {
		// check that there is at least 2 arg (search + tag / look + tag)
		visuals.Error("Wrong usage ! Not enough argument!")
		core.Help()
		return
	}

	// Handle args				~~~~~~~~~~~~~~~~~~~~~~~~
	for _, a := range os.Args[1:] {
		switch a {
		case "search":
			// search 			----------------
			gathering = true
			MODE = M_SEARCH
			continue
		case "look":
			gathering = true
			MODE = M_LOOK
		default:
			// gather search	----------------
			if gathering {
				// mode options	--------
				switch MODE {
				case M_SEARCH:
					// exact mode  - - -
					if a == "-e" && MODE == M_SEARCH {
						exact_mode = true
						continue
					}

					// all mode  - - - -
					if a == "-a" && MODE == M_SEARCH {
						search = ""
						gathering = false
						continue
					}

					// flag search mode
					if a == "-f" && MODE == M_SEARCH {
						MODE = M_SEARCH_FLAG
						continue
					}
				}

				// search tag	 - - - -
				search = a
				gathering = false
			} else {
				// print error if not gathering a search string
				visuals.Error("Wrong usage! " + a + " not recognised!")
				core.Help()
				return
			}
		}
	}

	// process					~~~~~~~~~~~~~~~~~~~~~~~~
	switch MODE {
	case M_SEARCH:
		// search by name		----------------
		full_bin_list := bins.GatherBinaries()
		resulted_bin_list := bins.SearchBinName(full_bin_list, search, exact_mode)
		if len(resulted_bin_list) == 0 {
			visuals.Error("\tNo result found from \"" + search + "\" name search.")
		} else {
			bins.PrintBins(resulted_bin_list)
			visuals.SubTitle("\tFound " + strconv.Itoa(len(resulted_bin_list)) + " result(s) from \"" + search + "\" name search.")
		}
	case M_SEARCH_FLAG:
		// search by flag		----------------
		full_bin_list := bins.GatherBinaries()
		resulted_bin_list := bins.SearchBinFlag(full_bin_list, search)
		if len(resulted_bin_list) == 0 {
			visuals.Error("\tNo result found from \"" + search + "\" flag search.")
		} else {
			bins.PrintBins(resulted_bin_list)
			visuals.SubTitle("\tFound " + strconv.Itoa(len(resulted_bin_list)) + " result(s) from \"" + search + "\" flag search.")
		}
	case M_LOOK:
		// look					----------------
		bins.PrintBinPage(search)
	}
}
