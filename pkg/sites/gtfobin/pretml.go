package gtfobin

import "strings"

/*
	GOtfo site/gtfobin
	Pretty HTML is used to clean scrapped html page (here from https://gtfobins.github.io/)
	M3GALOADON - 2022
*/

func FilterResponse(text string) string {
	// filter weird outputs
	text = strings.Replace(text, "&#39;", "'", -1)
	text = strings.Replace(text, "&quot;", "\"", -1)
	text = strings.Replace(text, "&lt;", "<", -1)
	text = strings.Replace(text, "&gt;", ">", -1)
	text = strings.Replace(text, "&amp;", "&", -1)

	// handle code
	if strings.Contains(text, "<code class=\"language-plaintext highlighter-rouge\">") {
		text = strings.Replace(text, "<code class=\"language-plaintext highlighter-rouge\">", "\033[38;5;196m\033[1m", -1)
		text = strings.Replace(text, "</code>", "\033[0m", -1)
	}

	// return the text
	return text
}
