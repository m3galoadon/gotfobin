package gtfobin

import (
	"GOtfo/pkg/core/visuals"
	"bufio"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

/*
	GOtfo site/gtfobin
	Module made to look a binary information from gtfobins.github.io
	M3GALOADON - 2022
*/

// return the URL of the binary page
func BinUrl(bin_name string) string {
	return "https://gtfobins.github.io/gtfobins/" + bin_name + "/"
}

// GOtfo look					///////////////////////////////////////////////////////////////////////////////////////////////////////////
func LookPage(search string) string {
	/*	Will show the page of the desired binary (in terminal)	*/
	var result string = ""

	// make the url				~~~~~~~~~~~~~~~~~~~~~~~~
	var url_addr string = BinUrl(search)

	// make a GET request		~~~~~~~~~~~~~~~~~~~~~~~~
	response, err := http.Get(url_addr)
	if err != nil {
		log.Fatal(err)
	}

	// check if response is 200	~~~~~~~~~~~~~~~~~~~~~~~~
	if response.StatusCode != 200 {
		visuals.Error("\tBinary page of \"" + search + "\" not found!")
		return ""
	}

	// treat body				~~~~~~~~~~~~~~~~~~~~~~~~
	bytes, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		log.Fatal(errRead)
	}

	// get all the lines		~~~~~~~~~~~~~~~~~~~~~~~~
	scanner := bufio.NewScanner(strings.NewReader(string(bytes)))
	var code_example bool = false

	// iterate through every lines	~~~~~~~~~~~~~~~~~~~~
	for scanner.Scan() {
		// HTML INTERPRETER		------------------------
		if strings.Contains(scanner.Text(), "function-name") {
			var title_name string = strings.Split(scanner.Text(), "function-name\">")[1]
			title_name = strings.Split(title_name, "</h2>")[0]
			if result != "" {
				result += "\n"
			}
			result += visuals.FlagTitleStr(title_name)
		} else if strings.Contains(scanner.Text(), "</p>") {
			var text string = strings.Split(scanner.Text(), "<p>")[1]
			text = strings.Split(text, "</p>")[0]
			text = FilterResponse(text)
			result += text + "\n"
		} else if strings.Contains(scanner.Text(), "<code>") {
			code_example = true
		}

		// if printing an example code------------------
		if code_example {
			// get initial text	--------
			var text string = scanner.Text()

			// filter text
			text = FilterResponse(text)

			// ensure text is not empty
			if text == "" {
				continue
			}

			//  code start filter	----
			if strings.Contains(text, "<code>") {
				text = strings.Split(text, "<code>")[1]
			}

			// code end filter	--------
			if strings.Contains(text, "</code>") {
				text = strings.Split(text, "</code>")[0]
				code_example = false
			}

			// code				--------
			result += visuals.CodeStr(text)

			// new line			--------
			if !code_example {
				result += "\n"
			}
		}
	}

	// return full page
	return result
}
