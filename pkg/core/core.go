package core

import (
	"GOtfo/pkg/core/visuals"
	"fmt"
)

/*
	GOtfo core module
	Used to handle core functions as Help() and Version()
	M3GALOADON - 2022
*/

// VERSION						~~~~~~~~~~~~~~~~~~~~~~~~
const GOTFO_VERSION string = "0.3.0"

// Show version
func Version() {
	fmt.Printf(visuals.Header() + "- version " + visuals.COLOR_BOLD + GOTFO_VERSION + visuals.COLOR_RESET + "\n")
}

// Help menu
func Help() {
	/*	Prints the help page	*/

	// Version					~~~~~~~~~~~~~~~~~~~~~~~~
	Version()

	// base						~~~~~~~~~~~~~~~~~~~~~~~~
	visuals.Title("HELP MENU:")
	fmt.Printf("\tGOtfo (GO The Fuck Out bins) is a utility to fast-scrap informations from gtfobins.io.\n")
	fmt.Printf("\tIt is used to get informations about some usual binaries and ways to escalate your \n")
	fmt.Printf("\tprivilege from them on a unix-like machine.\n")
	fmt.Printf("\n")

	// usage					~~~~~~~~~~~~~~~~~~~~~~~~
	fmt.Println(visuals.COLOR_BOLD + "\tUsage:" + visuals.COLOR_RESET)
	fmt.Printf("\t\tgotfo <MODE> <OPTIONS> <BIN-NAME>\n")
	fmt.Printf("\n")

	// options 					~~~~~~~~~~~~~~~~~~~~~~~~
	fmt.Println(visuals.COLOR_BOLD + "\tModes:" + visuals.COLOR_RESET)
	fmt.Printf("\t\tsearch\t\t: Will look for a binary with <BIN-NAME> name on the site.\n")
	fmt.Printf("\t\t\t-e\t: Exact mode option. Will look only for this exact binary. (gotfo search -e cp)\n")
	fmt.Printf("\t\t\t-a\t: All option. Will retrieve all binaries. (works only without binary name) (gotfo search -a)\n")
	fmt.Printf("\t\t\t-f\t: Search by flag option. Will look for binaries with the specified flag. (gotfo search -f sudo) \n")
	fmt.Printf("\t\tlook\t\t: Will show the <BIN-NAME> page from the site.\n")
	fmt.Printf("\n")

	// misc						~~~~~~~~~~~~~~~~~~~~~~~~
	fmt.Println(visuals.COLOR_BOLD + "\tMiscelanious:" + visuals.COLOR_RESET)
	fmt.Printf("\t\t--help\t\t: Print this help menu.\n")
	fmt.Printf("\t\t--version\t: Print the gotfo version.\n")
	fmt.Printf("\n")
}
