package bins

import (
	"GOtfo/pkg/core/visuals"
	"GOtfo/pkg/sites/gtfobin"
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

/*
	GOtfo core/bins
	Used to handle binaries informations (Name and Vulnerability flags)
	M3GALOADON - 2022
*/

// Binary struct
type Binary struct {
	Name  string   // Name of the binary (e.g: bash)
	Flags []string // Flags of the binary (e.g: sudo)
}

/*	Gather all the binaries informations from gtfobins.github.io		*/
func GatherBinaries() []Binary {
	/*	Will gather all the binaries 	*/

	// init
	var result []Binary

	// make a GET request		~~~~~~~~~~~~~~~~~~~~~~~~
	response, err := http.Get("https://gtfobins.github.io/")
	if err != nil {
		log.Fatal(err)
	}

	// treat body				~~~~~~~~~~~~~~~~~~~~~~~~
	bytes, errRead := ioutil.ReadAll(response.Body)
	if errRead != nil {
		log.Fatal(errRead)
	}

	// get all the bin names	~~~~~~~~~~~~~~~~~~~~~~~~
	scanner := bufio.NewScanner(strings.NewReader(string(bytes)))

	// iterate through every lines	~~~~~~~~~~~~~~~~~~~~
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "bin-name") {
			// Get the binary name
			first_split := strings.Split(scanner.Text(), "class=\"bin-name\">")[1]
			second_split := strings.Split(first_split, "</a>")[0]
			result = append(result, Binary{Name: second_split})
		} else if strings.Contains(scanner.Text(), "<li><a href=\"/gtfobins/") {
			// Get flags
			first_split := strings.Split(scanner.Text(), "/#")[1]
			second_split := strings.Split(first_split, "\">")[0]
			result[len(result)-1].Flags = append(result[len(result)-1].Flags, second_split)
		}
	}

	return result
}

/*	Search by name into the binary list 	*/
func SearchBinName(binaries []Binary, search string, exact_mode bool) []Binary {
	// init
	var result []Binary

	// log
	if exact_mode {
		visuals.Title("Looking for a binary named \"" + search + "\":")
	} else {
		visuals.Title("Looking for binaries containing \"" + search + "\":")
	}

	// iterate through the binaries
	for _, bin := range binaries {
		if exact_mode {
			if bin.Name == search {
				result = append(result, bin)
			}
		} else {
			if strings.Contains(bin.Name, search) {
				result = append(result, bin)
			}
		}
	}

	// return the result
	return result
}

/*	Search by flag into the binary list		*/
func SearchBinFlag(binaries []Binary, search string) []Binary {
	// init
	var result []Binary

	// log
	visuals.Title("Looking for binaries containing the \"" + search + "\" flag:")

	// iterate through binaries flags
	for _, bin := range binaries {
		for _, flag := range bin.Flags {
			if flag == search {
				result = append(result, bin)
			}
		}
	}

	// return the result
	return result
}

// PrintBin prints the full binary list gathered
func PrintBins(binList []Binary) {
	// titles
	visuals.BinName("Binary name")
	visuals.BinName("Flags")
	visuals.NewLine()

	// print bins
	for _, item := range binList {
		visuals.BinName(item.Name)
		for _, flag := range item.Flags {
			visuals.Flag(flag)
		}
		visuals.NewLine()
	}
}

// PrintBinPage prints the full page of the binary (located into the bin struct)
func PrintBinPage(bin_name string) {
	visuals.Title(bin_name + " page:")
	fmt.Print(gtfobin.LookPage(bin_name))
}
