package visuals

import (
	"fmt"

	"golang.org/x/term"
)

/*
	GOtfo core/visuals
	Used to get pretty look into the linux/unix command line interpreter
	M3GALOADON - 2022
*/

// Global variables				///////////////////////////////////////////////////////////////////////////////////////////////////////////
// COLORS BASE					~~~~~~~~~~~~~~~~~~~~~~~~
const (
	COLOR_BOLD      = "\033[1m"
	COLOR_TITLE     = COLOR_BOLD + "\033[48;5;255m\033[38;5;232m"
	COLOR_RED       = "\033[38;5;196m"
	COLOR_FLAG      = "\033[48;5;88m\033[38;5;255m"
	COLOR_RESET     = "\033[0m"
	COLOR_UNDERLINE = "\033[4m"
	COLOR_CODE      = "\033[48;5;235m\033[38;5;253m"
)

// Fill the remaining place with space (from x/term)
func FillTermStr(text string) string {
	// init
	var result string = ""
	tw, _, err := term.GetSize(0)
	if err != nil {
		return " ~ Error loading terminal size! (visuals.FillTermStr())"
	}

	// add space
	for i := 0; i < (tw - (len(text))); i++ {
		result += " "
	}

	return result
}

// Title prints a title
func Title(text string) {
	fmt.Printf(COLOR_TITLE + " " + text + FillTermStr(text+" ") + COLOR_RESET + "\n")
}

// return a formatted title
func FlagTitleStr(text string) string {
	return (COLOR_FLAG + COLOR_BOLD + " " + text + FillTermStr(text+" ") + COLOR_RESET + "\n")
}

// SubTitle prints a subtitle
func SubTitle(text string) {
	fmt.Printf(COLOR_UNDERLINE + text + COLOR_RESET + "\n")
}

// Error prints an error
func Error(text string) {
	fmt.Printf(COLOR_BOLD + COLOR_RED + text + COLOR_RESET + "\n")
}

// Flag print a formatted flag string (no new line)
func Flag(text string) {
	fmt.Printf(COLOR_FLAG+" %+14s "+COLOR_RESET+" ", text)
}

// BinItemName print a formatted binary name
func BinName(text string) {
	fmt.Printf("\t%-16s\t", text)
}

// NewLine print a newline
func NewLine() {
	fmt.Println()
}

// CodeStr return a formatted code string
func CodeStr(text string) string {
	return (COLOR_CODE + "  " + text + FillTermStr("  "+text) + COLOR_RESET + "\n")
}

// Header prints the header
func Header() string {
	return ("GOtfo " + COLOR_RED + "#" + COLOR_RESET)
}
