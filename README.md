# GOtfo (GO The F**k Out)
GOtfo is a simple CLI program for retrieving informations from [https://gtfobins.github.io/]. It is used to easily find binary vulnerabilities for unix and unix-based systems.

## Build
* Using GO :
``go build main -o gotfo``

* Using built-in script :
``./build.sh``

## Usage
Search some vulnerable binaries:
``gotfo search cp``

Search one exact binary:
``gotfo search -e cp``

Querry all vulnerable binaries:
``gotfo search -a``

Using grep with gtfo (example finding binaries vulnerable using sudo)
``gotfo search -a | grep 'sudo'``

More commands:
``gotfo --help``